# v2k5_1DCA_VIZ

a quick & unsophisticated Cellular Automata visualiser for 1 dimensional v2k5 rules, aka 5 neighbour binary. program written in [Processing](http://www.processing.org) language. 

 key presses are listed in code
 
 the v2k5 space is very forgiving, just keep presssing r and see what you get :]
 
 this is based on some ancient code, slightly updated Jan 2020.
 its a handy way to be able to look at ca spacetime dynamics.
 also useful for looking at the core algorithm for 1D binary CA without having to wade through a load of other code.
 this method of computing CA, generally called old cells / new cells algorithm is likely one of the oldest methods 
 and has prob been around since before i was born..
  
 some of my old CA research papers are hopefully archived on =>
 https://web.archive.org/web/20181216054859/http://www.noyzelab.com/research/research.html
 as i am no longer running the old noyzelab server. 
 
 my phd thesis 'Generative Music & Cellular Automata' is still accessible via link on my site bio page:
 https://www.noyzelab.com/bio.html
 which contains tons of CA refs
 
 as usual i recommend anyone wanting to delve deeper into CA to go to Andy Wuensche's site =>
 www.ddlab.org
  read my review of Andy's book Exploring Discrete Dynamics, originally published in Journal of Cellular Automata vol 8 no 1-2, 2013
 http://www.ddlab.org/#Reviews
  
 if u find this prog useful please think about supporting my work either thru my bandcamp page:
 https://noyzelab.bandcamp.com/
 or get in touch via noyzelab [at] gmail [dot] com 
 thanks, dave