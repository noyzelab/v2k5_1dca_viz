 // 1D Cellular Automata visualiser
 // Dave Burraston 
 // www.noyzelab.com
 //
 // 
 // 
 // a quick & unsophisticated CA visualiser for 1 dimensional v2k5 rules, aka 5 neighbour binary 
 //
 // key presses are listed in code
 //
 // the v2k5 space is very forgiving, just keep presssing r and see what you get :]
 //
 // this is based on some ancient code, slightly updated Jan 2020.
 // its a handy way to be able to look at ca spacetime dynamics.
 // also useful for looking at the core algorithm for 1D binary CA without having to wade through a load of other code.
 // this method of computing CA, generally called old cells / new cells algorithm is likely one of the oldest methods 
 // and has prob been around since before i was born..
 // 
 // some of my old CA research papers are hopefully archived on =>
 // https://web.archive.org/web/20181216054859/http://www.noyzelab.com/research/research.html
 // as i am no longer running the old noyzelab server. 
 //
 // my phd thesis 'Generative Music & Cellular Automata' is still accessible via link on my site bio page:
 // https://www.noyzelab.com/bio.html
 // which contains tons of CA refs
 //
 // as usual i recommend anyone wanting to delve deeper into CA to go to Andy Wuensche's site =>
 // www.ddlab.org
 // + read my review of Andy's book Exploring Discrete Dynamics, originally published in Journal of Cellular Automata vol 8 no 1-2, 2013
 // http://www.ddlab.org/#Reviews
 //
 // **********************************************************************************************
 // if u find this prog useful please think about supporting my work either thru my bandcamp page:
 // https://noyzelab.bandcamp.com/
 // or get in touch via noyzelab@gmail.com 
 // thanks, dave
 
 
int Lsize = 16;
int maxLsize = 500;   
int cellsize = 3;
int celloutsize = 72;

long rnum =  0xFF00FF00; 


int rtabsize = 32; // NEEDS to be 32 for v2k5
int [] rtab = new int[32]; 
int rtablkup = 0;

int k = 5;
int rndword1,rndword2; // for random rule table lookup


int [] oldcells = new int[600];
int [] newcells = new int[600]; 


int gens = 0;
PFont f;

void setup(){
  
  f = createFont("Georgia", 24);
  textFont(f);

  size(1200, 800);
  frameRate(25);  
  background(0);
       clearca();
        oldcells[15] = 1;         
 
      randruleinit();
        Lsize =  int(random(50));
        carandinit();
        text("Lsize : ", 10, 620);
  text(Lsize, 80, 620);   
    
}

void draw(){ 
    castep();    
  delay(100);   
}

void keyPressed() 
{
  
switch(key) {
  case '1':
  rnum =  0xFF00FF00; // right shift
  ruleinit();
  break;
  case '2':
  rnum =  0xcccccccc; // left shift
  ruleinit();
  break;  
  case '3':
  rnum =  0xf0f0f0f0; // all off
  ruleinit();
  clearca();
  break;  
  case '4':
  rnum =  0xffffffff; // all on
  ruleinit();
  break;    
  case '5':
  rnum =  0x3c3c3c3c; // rule 102  
  ruleinit();
  break;  
  case 'a': // single seed
  clear();  
  clearca();
  oldcells[Lsize-1] = 1; 
  break;

  case 's': // new random seed
  clear();  
  carandinit();
  break;
  case 'r': // randomize all
  clear();  
  randruleinit();
  Lsize =  int(random(maxLsize));
  carandinit();
  text("Lsize : ", 10, 620);
  text(Lsize, 80, 620);  
  print("LSIZE: ");
  print(Lsize);
  println();
    break;
  default:             
    println("None");   
    break;
  }
}

// ********* RULE INIT *********
// this inits a rule directly form a 32 bit unsigned long
void ruleinit()
{
 int ri,i;  
  for (ri = 0; ri < rtabsize; ri = ri + 1) 
    {
      rtab[ri] = byte((rnum >> ri) & 1);
     }    
  // ********* RTAB  DISPLAY *********
      print("RTAB: ");
      for (i = 0; i < 32; i = i + 1) 
    {
      print( rtab[i]);
    }
      println();
}
// ********* end RULE INIT *********

// ********* RANDOM RULE INIT *********
// this inits ruletab from two random unsigned ints, because arduino is so shite it can;t make a 32 unsigned random number  

void randruleinit()
{
   int rloop, i;
    for (rloop = 0; rloop < rtabsize; rloop = rloop + 1) 
    {
      rtab[rloop] = int(random(2));
    }   
       
// ********* RTAB  DISPLAY *********
      print("RTAB: ");
      for (i = 0; i < 32; i = i + 1) 
    {
      print( rtab[i]);
    }
      println();
}


// ********* CLEAR CA  *********
void clearca()
{
  int si; 
  gens = 0;  
  for (si = 0; si < Lsize; si = si + 1) 
    {
      oldcells[si] = 0;  
    };
  
}
// ********* end CLEAR CA *********



// ********* SET CA  *********
void setca()
{
  int si; 
  gens = 0;  
  for (si = 0; si < Lsize; si = si + 1) 
    {
      oldcells[si] = 1;  
    };
}
// ********* end SET CA *********


// ********* CA RAND INIT *********
void carandinit()
{
  int si; 
  gens = 0;  
  for (si = 0; si < Lsize; si = si + 1) 
    {
      oldcells[si] = int(random(2));  
    };
  
}
// ********* end CA RAND INIT *********

// ********* CA STEP *********
void castep()
{
int i,lfttmp,righttmp,nblkup;
 
   // compute ca step   
     for (i = 0; i < Lsize; i = i + 1) // do all cells
      {        
        rtablkup = 16*oldcells[(i-2+Lsize)%Lsize] + 8*oldcells[(i-1+Lsize)%Lsize] + 4*oldcells[i] + 2 * oldcells[(i+1)%Lsize] + oldcells[(i+2)%Lsize]; // v2k5 neighbourhood lookups 
        newcells[i] = rtab[rtablkup];// do table lookups
      }// end do all cells
      
       // copy current generation to old cells  
  for (i = 0; i < Lsize; i = i + 1) 
    {
       oldcells[i] = newcells[i];
    }
   gens = gens +1;
        
  // ********* CA DISPLAY *********
      for (i = 0; i < Lsize; i = i + 1) 
    {
      if (boolean(newcells[i]))
      {
        fill(255);
      }
      else
      {
      fill(0);
      }
      rect(i*cellsize,(gens%((height-200)/cellsize))*cellsize,cellsize,cellsize);
    }

 // ********* CA LOGIC CELL OUT DISPLAY *********

            stroke(204, 102, 0);  
   for (i = 0; i < 64; i = i + 1) 
    {
      if (boolean(newcells[i]))
      {
        fill(255);
      }
      else
      {
      fill(0);
      }
      rect((i*celloutsize/4)+20,((height-128)/celloutsize)*celloutsize,celloutsize/4,celloutsize/4);
    }      
    stroke(0);

 // CONSOLE UP TO 80 columns
/*      for (i = 0; i < 80; i = i + 1) 
    {
       print( newcells[i]);
       
    }
      println();
 */
       
// ********* CA DISPLAY LARGE*********
/*
      for (i = 0; i < Lsize; i = i + 1) 
    {
       Serial.print( newcells[i]);
       
    }
Serial.println();  
  */  
}
// ********* end CA STEP *********


      